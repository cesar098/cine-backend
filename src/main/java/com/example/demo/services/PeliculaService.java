package com.example.demo.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Optional;
import java.net.MalformedURLException;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import com.example.demo.models.PeliculaModel;
import com.example.demo.repositories.PeliculaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class PeliculaService {
    @Autowired
    PeliculaRepository peliculaRepository;
    
    private final Path root = Paths.get("files");

    
    public void save(MultipartFile file) {
        try {
            //copy (que queremos copiar, a donde queremos copiar)
            Files.copy(file.getInputStream(), 
                       this.root.resolve(file.getOriginalFilename()));
        } catch (IOException e) {
            throw new RuntimeException("No se puede guardar el archivo. Error " + e.getMessage());
        }
    }

    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if(resource.exists() || resource.isReadable()){
                return resource;
            }else{
                throw new RuntimeException("No se puede leer el archivo ");
            }

        }catch (MalformedURLException e){
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    public ArrayList<PeliculaModel> obtenerPeliculas(){
        return (ArrayList<PeliculaModel>) peliculaRepository.findAll();
    }

    public PeliculaModel guardarPelicula(PeliculaModel pelicula){
        return peliculaRepository.save(pelicula);
    }

    public Optional<PeliculaModel> obtenerPorId(Long id){
        return peliculaRepository.findById(id);
    }


    public ArrayList<PeliculaModel>  obtenerPorStatus(Integer status) {
        return peliculaRepository.findByStatus(status);
    }

    public boolean eliminarPelicula(Long id) {
        try{
            peliculaRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }    
}