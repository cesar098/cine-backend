package com.example.demo.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Arrays;
import java.util.List;

import com.example.demo.models.PeliculaModel;
import com.example.demo.services.PeliculaService;

import org.springframework.http.HttpHeaders;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/pelicula")
public class PeliculaController {
    @Autowired
    PeliculaService peliculaService;

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFiles(@RequestParam("files")MultipartFile[] files){
        String message = "";
        try{
            List<String> fileNames = new ArrayList<>();

            Arrays.asList(files).stream().forEach(file->{
                peliculaService.save(file);
                fileNames.add(file.getOriginalFilename());
            });

            message = "Se subieron los archivos correctamente " + fileNames;
            return ResponseEntity.status(HttpStatus.OK).body(message);
        }catch (Exception e){
            message = "Fallo al subir los archivos";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource> getFile(@PathVariable String filename){
        Resource file = peliculaService.load(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\""+file.getFilename() + "\"").body(file);
    }

    @GetMapping()
    public ArrayList<PeliculaModel> obtenerPeliculas(){
        return peliculaService.obtenerPeliculas();
    }

    @PostMapping()
    public PeliculaModel guardarPelicula(@RequestBody PeliculaModel pelicula){
        return this.peliculaService.guardarPelicula(pelicula);
    }

    @GetMapping( path = "/{id}")
    public Optional<PeliculaModel> obtenerPeliculaPorId(@PathVariable("id") Long id) {
        return this.peliculaService.obtenerPorId(id);
    }

    @GetMapping("/query")
    public ArrayList<PeliculaModel> obtenerPeliculaPorStatus(@RequestParam("prioridad") Integer status){
        return this.peliculaService.obtenerPorStatus(status);
    }

    @DeleteMapping( path = "/{id}")
    public String eliminarPorId(@PathVariable("id") Long id){
        boolean ok = this.peliculaService.eliminarPelicula(id);
        if (ok){
            return "Se eliminó el usuario con id: " + id;
        }else{
            return "No pudo eliminar el usuario con id: " + id;
        }
    }

}